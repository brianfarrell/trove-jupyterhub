# syntax=docker/dockerfile:1

FROM jupyterhub/jupyterhub:latest

LABEL fm.trove.image.name="JupyterHub for System Users"
LABEL fm.trove.image.authors="Brian Farrell <brian.farrell@me.com>"

RUN mkdir /srv/jupyterhub_config
WORKDIR /srv/jupyterhub_config

# Install psycopg2
RUN apt-get update
RUN apt-get -y install apt-utils gcc python3-dev libpq-dev
RUN pip install psycopg2

# Install dockerspawner and oauthenticator
# We need to update pip, otherwise the version of requests that
# is installed by dockerspawner breaks things.
RUN pip install --upgrade pip dockerspawner jupyterhub-idle-culler

# add the userlist, spawner, and authenticator
ADD jupyterhub_config.py /srv/jupyterhub_config/jupyterhub_config.py

# Copy the auth files to the container
COPY --from=ids group /etc/group
COPY --from=ids passwd /etc/passwd
COPY --from=ids shadow /etc/shadow

ENV NOTEBOOK_ARGS="--SingleUserNotebookApp.default_url=/lab"

# we need to expose ports for the hub api and for the proxy api
EXPOSE 8080
EXPOSE 8001
EXPOSE 8081

# run jupyterhub
ENTRYPOINT ["jupyterhub", "-f", "/srv/jupyterhub_config/jupyterhub_config.py"]
