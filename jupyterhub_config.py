
# Configuration file for jupyterhub.

import os


def getenv_bool(envar, default=None):
    value = os.getenv(envar, default)

    if isinstance(value, bool):
        return value
    elif value is None:
        raise ValueError(f"{envar} is not set")
    else:
        value = value.lower()

    if value in ('y', 'yes', 't', 'true', 'on', '1'):
        return True
    elif value in ('n', 'no', 'false', 'off', '0'):
        return False
    else:
        raise ValueError(f"{envar} does not evaluate to a boolean value: {value}")


def get_admins():
    admins = os.getenv('ADMIN_USERS')
    if admins is None:
        return set()
    else:
        admins = admins.split(',')
        return {admin.strip() for admin in admins}


c = get_config()  # noqa

# ------------------------------------------------------------------------------
# Authenticator configuration
# ------------------------------------------------------------------------------
## Base class for implementing an authentication provider for JupyterHub

## Set of users that will have admin rights on this JupyterHub.
#
#  Note: As of JupyterHub 2.0, full admin rights should not be required, and more
#  precise permissions can be managed via roles.
#
#  Admin users have extra privileges:
#   - Use the admin panel to see list of users logged in
#   - Add / remove users in some authenticators
#   - Restart / halt the hub
#   - Start / stop users' single-user servers
#   - Can access each individual users' single-user server (if configured)
#
#  Admin access should be treated the same way root access is.
#
#  Defaults to an empty set, in which case no user has admin access.
#  Default: set()
c.Authenticator.admin_users = get_admins()

## Delete any users from the database that do not pass validation
#
#          When JupyterHub starts, `.add_user` will be called
#          on each user in the database to verify that all users are still valid.
#
#          If `delete_invalid_users` is True,
#          any users that do not pass validation will be deleted from the database.
#          Use this if users might be deleted from an external system,
#          such as local user accounts.
#
#          If False (default), invalid users remain in the Hub's database
#          and a warning will be issued.
#          This is the default to avoid data loss due to config changes.
#  Default: False
c.Authenticator.delete_invalid_users = getenv_bool('DELETE_INVALID_USERS', False)


# ------------------------------------------------------------------------------
# JupyterHub configuration
# ------------------------------------------------------------------------------
## An Application for starting a Multi-User Jupyter Notebook server.

####################
# Database
####################

## Include any kwargs to pass to the database connection.
#          See sqlalchemy.create_engine for details.
#  Default: {}
# c.JupyterHub.db_kwargs = {}

## url for the database. e.g. `sqlite:///jupyterhub.sqlite`
#  Default: 'sqlite:///jupyterhub.sqlite'
# c.JupyterHub.db_url = 'sqlite:///jupyterhub.sqlite'
POSTGRES_DB = os.getenv('POSTGRES_DB')
POSTGRES_USER = os.getenv('POSTGRES_USER')
POSTGRES_PASSWORD = os.getenv('POSTGRES_PASSWORD')
POSTGRES_SERVER = os.getenv('POSTGRES_SERVER')
POSTGRES_PORT = os.getenv('POSTGRES_PORT')

c.JupyterHub.db_url = (
    f"postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}"
    f"@{POSTGRES_SERVER}:{POSTGRES_PORT}/{POSTGRES_DB}"
)

## log all database transactions. This has A LOT of output
#  Default: False
c.JupyterHub.debug_db = getenv_bool('DEBUG_DB', False)

## Purge and reset the database.
#  Default: False
c.JupyterHub.reset_db = getenv_bool('RESET_DB', False)

## Upgrade the database automatically on start.
#
#          Only safe if database is regularly backed up.
#          Only SQLite databases will be backed up to a local file automatically.
#  Default: False
c.JupyterHub.upgrade_db = getenv_bool('UPGRADE_DB', False)


####################
# Hub
####################

## Maximum number of concurrent servers that can be active at a time.
#
#  Setting this can limit the total resources your users can consume.
#
#  An active server is any server that's not fully stopped. It is considered
#  active from the time it has been requested until the time that it has
#  completely stopped.
#
#  If this many user servers are active, users will not be able to launch new
#  servers until a server is shutdown. Spawn requests will be rejected with a 429
#  error asking them to try again.
#
#  If set to 0, no limit is enforced.
#  Default: 0
c.JupyterHub.active_server_limit = int(os.getenv('ACTIVE_SERVER_LIMIT', 0))

## The public facing URL of the whole JupyterHub application.
#
#          This is the address on which the proxy will bind.
#          Sets protocol, ip, base_url
#  Default: 'http://:8000'
c.JupyterHub.bind_url = os.getenv('BIND_URL', 'http://:8000')

# Whether to shutdown the proxy when the Hub shuts down.
#
#          Disable if you want to be able to teardown the Hub while leaving the
#  proxy running.
#
#          Only valid if the proxy was starting by the Hub process.
#
#          If both this and cleanup_servers are False, sending SIGINT to the Hub will
#          only shutdown the Hub, leaving everything else running.
#
#          The Hub should be able to resume from database state.
#  Default: True
c.JupyterHub.cleanup_proxy = getenv_bool('CLEANUP_PROXY', True)

## Whether to shutdown single-user servers when the Hub shuts down.
#
#          Disable if you want to be able to teardown the Hub while leaving the
#  single-user servers running.
#
#          If both this and cleanup_proxy are False, sending SIGINT to the Hub will
#          only shutdown the Hub, leaving everything else running.
#
#          The Hub should be able to resume from database state.
#  Default: True
c.JupyterHub.cleanup_servers = getenv_bool('CLEANUP_SERVERS', True)

## The config file to load
#  Default: 'jupyterhub_config.py'
c.JupyterHub.config_file = os.getenv('CONFIG_FILE', 'jupyterhub_config.py')

## Number of days for a login cookie to be valid.
#          Default is two weeks.
#  Default: 14
c.JupyterHub.cookie_max_age_days = int(os.getenv('COOKIE_MAX_AGE_DAYS', 14))

## The URL on which the Hub will listen. This is a private URL for internal
#  communication. Typically set in combination with hub_connect_url. If a unix
#  socket, hub_connect_url **must** also be set.
#
#  For example:
#
#      "http://127.0.0.1:8081"
#      "unix+http://%2Fsrv%2Fjupyterhub%2Fjupyterhub.sock"
#
#  .. versionadded:: 0.9
#  Default: ''
c.JupyterHub.hub_bind_url = os.getenv('HUB_BIND_URL', '')

## The class to use for spawning single-user servers.
#
#          Should be a subclass of :class:`jupyterhub.spawner.Spawner`.
#
#  Currently installed:
#    - default: jupyterhub.spawner.LocalProcessSpawner
#    - localprocess: jupyterhub.spawner.LocalProcessSpawner
#    - simple: jupyterhub.spawner.SimpleLocalProcessSpawner
#    - docker: dockerspawner.DockerSpawner
#    - docker-swarm: dockerspawner.SwarmSpawner
#    - docker-system-user: dockerspawner.SystemUserSpawner
#  Default: 'jupyterhub.spawner.LocalProcessSpawner'
c.JupyterHub.spawner_class = os.getenv('SPAWNER_CLASS', 'jupyterhub.spawner.LocalProcessSpawner')

## Downstream proxy IP addresses to trust.
#
#          This sets the list of IP addresses that are trusted and skipped when processing
#          the `X-Forwarded-For` header. For example, if an external proxy is used for TLS
#          termination, its IP address should be added to this list to ensure the correct
#          client IP addresses are recorded in the logs instead of the proxy server's IP
#          address.
#  Default: []
c.JupyterHub.trusted_downstream_ips = ['proxy']


####################
# Internal SSL
####################

## Generate certs used for internal ssl
#  Default: False
c.JupyterHub.generate_certs = getenv_bool('GENERATE_CERTS', False)

## Enable SSL for all internal communication
#
#          This enables end-to-end encryption between all JupyterHub components.
#          JupyterHub will automatically create the necessary certificate
#          authority and sign notebook certificates as they're created.
#  Default: False
c.JupyterHub.internal_ssl = getenv_bool('INTERNAL_SSL', False)

## Recreate all certificates used within JupyterHub on restart.
#
#          Note: enabling this feature requires restarting all notebook servers.
#
#          Use with internal_ssl
#  Default: False
c.JupyterHub.recreate_internal_certs = getenv_bool('RECREATE_INTERNAL_CERTS', False)

## Path to SSL certificate file for the public facing interface of the proxy
#
#          When setting this, you should also set ssl_key
#  Default: ''
c.JupyterHub.ssl_cert = os.getenv('INTERNAL_SSL_CERT', '')

## Path to SSL key file for the public facing interface of the proxy
#
#          When setting this, you should also set ssl_cert
#  Default: ''
c.JupyterHub.ssl_key = os.getenv('INTERNAL_SSL_KEY', '')

## Names to include in the subject alternative name.
#
#          These names will be used for server name verification. This is useful
#          if JupyterHub is being run behind a reverse proxy or services using ssl
#          are on different hosts.
#
#          Use with internal_ssl
#  Default: []
TRUSTED_ALT_NAMES = os.getenv('TRUSTED_ALT_NAMES')

if TRUSTED_ALT_NAMES:
    c.JupyterHub.trusted_alt_names = [name.strip() for name in TRUSTED_ALT_NAMES.split(',')]


####################
# Logging
####################

## Instead of starting the Application, dump configuration to stdout
c.JupyterHub.show_config = getenv_bool('SHOW_LOGGING_CONFIG', False)

## Instead of starting the Application, dump configuration to stdout (as JSON)
c.JupyterHub.show_config_json = getenv_bool('SHOW_LOGGING_CONFIG_JSON', False)


####################
# Metrics
####################

## Authentication for prometheus metrics
#  Default: True
c.JupyterHub.authenticate_prometheus = getenv_bool('AUTHENTICATE_PROMETHEUS', True)

## Host to send statsd metrics to. An empty string (the default) disables sending
#  metrics.
#  Default: ''
c.JupyterHub.statsd_host = os.getenv('STATSD_HOST', '')

## Port on which to send statsd metrics about the hub
#  Default: 8125
c.JupyterHub.statsd_port = int(os.getenv('STATSD_PORT', 8125))

## Prefix to use for all metrics sent by jupyterhub to statsd
#  Default: 'jupyterhub'
c.JupyterHub.statsd_prefix = os.getenv('STATSD_PREFIX', 'jupyterhub')


# ------------------------------------------------------------------------------
# Spawner configuration
# ------------------------------------------------------------------------------
## Base class for spawning single-user notebook servers.
#
#      Subclass this, and override the following methods:
#
#      - load_state
#      - get_state
#      - start
#      - stop
#      - poll
#
#      As JupyterHub supports multiple users, an instance of the Spawner subclass
#      is created for each user. If there are 20 JupyterHub users, there will be 20
#      instances of the subclass.

## Maximum number of consecutive failures to allow before shutting down
#  JupyterHub.
#
#  This helps JupyterHub recover from a certain class of problem preventing
#  launch in contexts where the Hub is automatically restarted (e.g. systemd,
#  docker, kubernetes).
#
#  A limit of 0 means no limit and consecutive failures will not be tracked.
#  Default: 0
c.Spawner.consecutive_failure_limit = int(os.getenv('SPAWNER_CONSECUTIVE_FAILURE_LIMIT', 0))

## Enable debug-logging of the single-user server
#  Default: False
# c.Spawner.debug = False

## The URL the single-user server should start in.
#
#  `{username}` will be expanded to the user's username
#
#  Example uses:
#
#  - You can set this to `/lab` to have JupyterLab start by default, rather than Jupyter Notebook.
#  Default: ''
c.Spawner.default_url = '/lab'

## Run Jupyter Lab by default in lab containers
c.Spawner.environment = {'JUPYTER_ENABLE_LAB': '1'}

# The image to use for single-user servers.
#
#  This image should have the same version of jupyterhub as the Hub itself
#  installed.
#
#  If the default command of the image does not launch jupyterhub-singleuser,
#  set `c.Spawner.cmd` to launch jupyterhub-singleuser, e.g.
#
#  Any of the jupyter docker-stacks should work without additional config, as
#  long as the version of jupyterhub in the image is compatible.
#  Default: 'jupyterhub/singleuser:1.3'
c.Spawner.image = os.getenv('DOCKER_JUPYTERLAB_IMAGE', 'jupyterhub/singleuser:1.3')

## The IP address (or hostname) the single-user server should listen on.
#
#  Usually either '127.0.0.1' (default) or '0.0.0.0'.
#
#  The JupyterHub proxy implementation should be able to send packets to this
#  interface.
#
#  Subclasses which launch remotely or in containers should override the default
#  to '0.0.0.0'.
#
#  Default: '127.0.0.1'
c.Spawner.ip = os.getenv('SPAWNER_IP', '127.0.0.1')

## Path to the notebook directory for the single-user server.
#
#  The user sees a file listing of this directory when the notebook interface is
#  started. The current interface does not easily allow browsing beyond the
#  subdirectories in this directory's tree.
#
#  `~` will be expanded to the home directory of the user, and {username} will be
#  replaced with the name of the user.
#
#  Note that this does *not* prevent users from accessing files outside of this
#  path! They can do so with many other means.
#  Default: ''
c.Spawner.notebook_dir = os.getenv('NOTEBOOK_DIR', '')

## Prefix for container names.
#
#  See name_template for full container name for a particular user’s server.
#  Default: 'jupyter'
c.Spawner.prefix = os.getenv('SPAWNER_PREFIX', 'jupyter')

## List of SSL alt names
#
#          May be set in config if all spawners should have the same value(s),
#          or set at runtime by Spawner that know their names.
#  Default: []
# c.Spawner.ssl_alt_names = []

## Whether to include DNS:localhost, IP:127.0.0.1 in alt names
#  Default: True
# c.Spawner.ssl_alt_names_include_local = True


# ------------------------------------------------------------------------------
# DockerSpawner configuration
# ------------------------------------------------------------------------------
# See https://jupyterhub-dockerspawner.readthedocs.io/en/latest/api/index.html

## Minimum number of cpu-cores a single-user notebook server is guaranteed to
#  have available.
#
#  If this value is set to 0.5, allows use of 50% of one CPU. If this value is
#  set to 2, allows use of up to 2 CPUs.
#
#  **This is a configuration setting. Your spawner must implement support for the
#  limit to work.** The default spawner, `LocalProcessSpawner`, does **not**
#  implement this support. A custom spawner **must** add support for this setting
#  for it to be enforced.
#  Default: None
# CPU_GUARANTEE = os.getenv('CPU_GUARANTEE')
#
# if CPU_GUARANTEE:
#     c.DockerSpawner.cpu_guarantee = float(CPU_GUARANTEE)

## Maximum number of cpu-cores a single-user notebook server is allowed to use.
#
#  If this value is set to 0.5, allows use of 50% of one CPU. If this value is
#  set to 2, allows use of up to 2 CPUs.
#
#  The single-user notebook server will never be scheduled by the kernel to use
#  more cpu-cores than this. There is no guarantee that it can access this many
#  cpu-cores.
#
#  **This is a configuration setting. Your spawner must implement support for the
#  limit to work.** The default spawner, `LocalProcessSpawner`, does **not**
#  implement this support. A custom spawner **must** add support for this setting
#  for it to be enforced.
#  Default: None
# CPU_LIMIT = os.getenv('CPU_LIMIT')
#
# if CPU_LIMIT:
#     c.Spawner.cpu_limit = float(CPU_LIMIT)

## Spawn the new lab container as root
c.DockerSpawner.extra_create_kwargs = {'user': '0'}

# The URL the single-user server should connect to the Hub.
c.DockerSpawner.hub_connect_url = os.getenv('HUB_CONNECT_URL')

# Specify docker link mapping to add to the container.  If the Hub is running in
# a Docker container, this can simplify routing because all traffic will be using docker hostnames.
c.DockerSpawner.links = {'jupyterhub': 'jupyterhub'}

## Minimum number of bytes a single-user notebook server is guaranteed to have
#  available.
#
#  Allows the following suffixes:
#    - K -> Kilobytes
#    - M -> Megabytes
#    - G -> Gigabytes
#    - T -> Terabytes
#
#  **This is a configuration setting. Your spawner must implement support for the
#  limit to work.** The default spawner, `LocalProcessSpawner`, does **not**
#  implement this support. A custom spawner **must** add support for this setting
#  for it to be enforced.
#  Default: None
# c.DockerSpawner.mem_guarantee = None

## Maximum number of bytes a single-user notebook server is allowed to use.
#
#  Allows the following suffixes:
#    - K -> Kilobytes
#    - M -> Megabytes
#    - G -> Gigabytes
#    - T -> Terabytes
#
#  If the single user server tries to allocate more memory than this, it will
#  fail. There is no guarantee that the single-user notebook server will be able
#  to allocate this much memory - only that it can not allocate more than this.
#
#  **This is a configuration setting. Your spawner must implement support for the
#  limit to work.** The default spawner, `LocalProcessSpawner`, does **not**
#  implement this support. A custom spawner **must** add support for this setting
#  for it to be enforced.
#  Default: None
# c.DockerSpawner.mem_limit = None

## Run the containers on this docker network.
#
#  If it is an internal docker network, the Hub should be on the same network,
#  as internal docker IP addresses will be used. For bridge networking,
#  external ports will be bound.
#  Default: 'bridge'
c.DockerSpawner.network_name = os.getenv('DOCKER_NETWORK_NAME', 'bridge')

# If True, delete containers when servers are stopped.
#
#  This will destroy any data in the container not stored in mounted volumes.
#  Default: False
c.DockerSpawner.remove = getenv_bool('SPAWNER_REMOVE_STOPPED_CONTAINERS', False)

## Configure Docker Networking
c.DockerSpawner.use_internal_ip = getenv_bool('DOCKER_USE_INTERNAL_IP', True)

## Map from host file/directory to container (guest) file/directory mount point
## and (optionally) a mode.
#
# When specifying the guest mount point (bind) for the volume, you may use a
#  dict or str. If a str, then the volume will default to a read-write
#  (mode="rw"). With a dict, the bind is identified by "bind" and the "mode"
#  may be one of "rw" (default), "ro" (read-only), "z" (public/shared SELinux
#  volume label), and "Z" (private/unshared SELinux volume label).
#
#  If format_volume_name is not set, default_format_volume_name is used for
#  naming volumes. In this case, if you use {username} in either the host or
#  guest file/directory path, it will be replaced with the current user’s name.
#  Default: {}
c.DockerSpawner.volumes = {'/home/{username}': os.getenv('NOTEBOOK_DIR', 'jupyter')}
